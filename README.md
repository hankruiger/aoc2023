## Run a solution

First, make sure you have the input file at `day<dd>/input` folder, then:

```
deno run --allow-read ./day<dd>/day<dd>.ts
```

## Run all tests

```zsh
deno test "**/*.ts"
```

import { assertEquals } from "https://deno.land/std@0.208.0/assert/assert_equals.ts";
import { reverse } from "../common/string-utils.ts";
import { isDigit } from "../common/digits.ts";
import { getLines } from "../common/io.ts";
import { assert } from "https://deno.land/std@0.208.0/assert/assert.ts";

type SchematicNumber = {
    value: number,
    row: number,
    span: Span,
}

type Gear = {
    ratio: number,
    row: number,
    col: number,
}

function isSymbol(c: string): boolean {
    return !isDigit(c) && c != '.' && c.length === 1;
}

type Span = [number, number];

function overlaps(a: Span, b: Span): boolean {
    return (
        a[0] <= b[0] && a[1] - 1 >= b[0] ||
        b[0] <= a[0] && b[1] - 1 >= a[0] ||
        a[0] >= b[0] && a[0] <= b[1] - 1 ||
        a[1] - 1 >= b[0] && a[1] - 1 <= b[1] - 1
    );
}

Deno.test('test span overlap', () => {
    assert(overlaps([0, 1], [0, 1]));
    assert(overlaps([0, 8], [1, 7]));
    assert(overlaps([0, 3], [2, 3]));
    assert(overlaps([0, 8], [7, 15]));
    assert(!overlaps([0, 8], [8, 15]));
    assert(!overlaps([8, 15], [0, 8]));
});

class EngineSchematic {
    constructor(private lines: string[]) { }

    getSchematicNumbers(): SchematicNumber[] {
        const result: SchematicNumber[] = [];

        this.lines.forEach((line, row) => {
            let consecutiveDigits = 0;
            let aggregateNumber = 0;
            [...reverse(line)].forEach((char, i) => {
                if (isDigit(char)) {
                    aggregateNumber += parseInt(char) * Math.pow(10, consecutiveDigits);
                    consecutiveDigits += 1;
                    if (i === line.length - 1) {
                        result.push({
                            value: aggregateNumber,
                            row,
                            span: [0, consecutiveDigits],
                        });
                    }
                } else if (consecutiveDigits > 0) {
                    result.push({
                        value: aggregateNumber,
                        row,
                        span: [line.length - i, line.length - i + consecutiveDigits],
                    });
                    aggregateNumber = 0;
                    consecutiveDigits = 0;
                }
            });
        });

        return result;
    }

    getPartNumbers() {
        return this.getSchematicNumbers().filter(sn => {
            if (sn.row > 0) {
                for (let i = Math.max(0, sn.span[0] - 1); i <= Math.min(sn.span[1], this.lines[sn.row - 1].length - 1); i++) {
                    if (isSymbol(this.lines[sn.row - 1][i])) return true;
                }
            }
            if (sn.span[0] > 0) {
                if (isSymbol(this.lines[sn.row][sn.span[0] - 1])) return true;
            }
            if (sn.span[1] <= this.lines[sn.row].length - 1) {
                if (isSymbol(this.lines[sn.row][sn.span[1]])) return true;
            }
            if (sn.row + 1 <= this.lines.length - 1) {
                for (let i = Math.max(0, sn.span[0] - 1); i <= Math.min(sn.span[1], this.lines[sn.row + 1].length - 1); i++) {
                    if (isSymbol(this.lines[sn.row + 1][i])) return true;
                }
            }
            return false;
        });
    }

    getSumOfPartNumbers(): number {
        return this.getPartNumbers().reduce((agg, pn) => agg + pn.value, 0);
    }

    getGears(): Gear[] {
        const partNumbers = this.getPartNumbers();
        const gears: Gear[] = [];
        this.lines.forEach((line, row) => {
            [...line].forEach((c, i) => {
                if (c !== '*') return;
                const gearXSpan = [i - 1, i + 2] as Span;
                const gearYSpan = [row - 1, row + 2] as Span;
                const nearbyPartNumbers = partNumbers.filter(pn =>
                    overlaps(gearYSpan, [pn.row, pn.row + 1]) && overlaps(gearXSpan, pn.span)
                );
                if (nearbyPartNumbers.length === 2) {
                    gears.push({
                        ratio: nearbyPartNumbers[0].value * nearbyPartNumbers[1].value,
                        row,
                        col: i,
                    });
                }
            });
        });
        return gears;
    }

    getSumOfGearRatios(): number {
        return this.getGears().reduce((agg, g) => agg + g.ratio, 0);
    }
}

Deno.test('day 3: part 1 example', () => {
    const exampleInput = `
        467..114..
        ...*......
        ..35..633.
        ......#...
        617*......
        .....+.58.
        ..592.....
        ......755.
        ...$.*....
        .664.598..
    `;
    const lines = exampleInput.trim().split('\n').map(l => l.trim());
    assertEquals(lines.length, 10);
    const schematic = new EngineSchematic(lines);
    const schematicNumbers = schematic.getSchematicNumbers();
    assertEquals(schematicNumbers.length, 10);
    schematicNumbers.forEach(sn => {
        assertEquals(`${sn.value}`, lines[sn.row].substring(sn.span[0], sn.span[1]));
    });
    const partNumbers = schematic.getPartNumbers();
    assertEquals(partNumbers.length, 8);
    assertEquals(getPart1Answer(schematic), 4361);
});

Deno.test('day 3: part 2 example', () => {
    const exampleInput = `
        467..114..
        ...*......
        ..35..633.
        ......#...
        617*......
        .....+.58.
        ..592.....
        ......755.
        ...$.*....
        .664.598..
    `;
    const lines = exampleInput.trim().split('\n').map(l => l.trim());
    const schematic = new EngineSchematic(lines);
    const gears = schematic.getGears();
    assertEquals(gears.length, 2);
    assertEquals(getPart2Answer(schematic), 467835);
});

function getPart1Answer(schematic: EngineSchematic) {
    return schematic.getSumOfPartNumbers();
}

function getPart2Answer(schematic: EngineSchematic) {
    return schematic.getSumOfGearRatios();
}

async function main() {
    const lineStream = await getLines("./day03/input");
    const lines = [];
    for await (const line of lineStream) {
        lines.push(line);
    }
    const schematic = new EngineSchematic(lines);
    const answer1 = await getPart1Answer(schematic);
    console.log(answer1);
    const answer2 = await getPart2Answer(schematic);
    console.log(answer2);
}

if (import.meta.main) await main();

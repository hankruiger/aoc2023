import { assertEquals } from "https://deno.land/std@0.208.0/assert/assert_equals.ts";
import { getLines } from "../common/io.ts";

class ScratchCardSpree {
    private readonly repeats = new Map<number, number>();
    totalScratches = 0;

    scratch(card: ScratchCard) {
        const repeatsForThisCard = 1 + (this.repeats.get(card.id) ?? 0);
        this.totalScratches += repeatsForThisCard;
        const wins = card.getNumberOfWinningNumbers();
        
        for (let i = 0; i < repeatsForThisCard; i++) {
            for (let j = card.id + 1; j <= card.id + wins; j++) {
                if (this.repeats.get(j) === undefined) this.repeats.set(j, 0);
                this.repeats.set(j, this.repeats.get(j)! + 1);
            }
        }
    }
}

class ScratchCard {
    constructor(
        public readonly id: number,
        public readonly winningNumbers: Set<number>,
        public readonly cardNumbers: Set<number>,
    ) {}

    static from(s: string) {
        const id = parseInt(
            s.split(':')[0]
                .split(' ')
                .filter(p => p !== '') // get rid of multiple spaces
            [1]
        );
        const winningNumbers = s.split(':')[1]
            .split('|')[0]
            .split(' ')
            .filter(p => p !== '') // get rid of multiple spaces
            .map(p => p.trim())
            .map(p => parseInt(p));
        const cardNumbers = s.split(':')[1]
            .split('|')[1]
            .split(' ')
            .filter(p => p !== '') // get rid of multiple spaces
            .map(p => p.trim())
            .map(p => parseInt(p));
        return new ScratchCard(id, new Set(winningNumbers), new Set(cardNumbers));
    }

    getNumberOfWinningNumbers () {
        return [...this.cardNumbers].filter(n => this.winningNumbers.has(n)).length;
    }

    getScore() {
        const numWinningNumbers = this.getNumberOfWinningNumbers();
        return numWinningNumbers > 0 ? Math.pow(2, numWinningNumbers - 1) : 0;
    }
}

async function getPart1Answer(lineStream: ReadableStream<string>): Promise<number> {
    let points = 0;
    for await (const line of lineStream) {
        const card = ScratchCard.from(line);
        points += card.getScore();
    }
    return points;
}

async function getPart2Answer(lineStream: ReadableStream<string>): Promise<number> {
    const spree = new ScratchCardSpree();
    for await (const line of lineStream) {
        const card = ScratchCard.from(line);
        spree.scratch(card);
    }
    return spree.totalScratches;
}

const EXAMPLE_DATA = `
    Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
    Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
    Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
    Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
    Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
    Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11
`;

Deno.test("day 4: part 1 example", async () => {
    const exampleData = ReadableStream.from(
        EXAMPLE_DATA
            .split('\n')
            .map(l => l.trim())
            .filter(l => l !== '') // get rid of multiple newlines
    );

    assertEquals(await getPart1Answer(exampleData), 13);
});

Deno.test("day 4: part 2 example", async () => {
    const exampleData = ReadableStream.from(
        EXAMPLE_DATA
            .split('\n')
            .map(l => l.trim())
            .filter(l => l !== '') // get rid of multiple newlines
    );

    assertEquals(await getPart2Answer(exampleData), 30);
});

async function main() {
    const lineStream1 = await getLines("day04/input");
    const answer1 = await getPart1Answer(lineStream1);
    console.log(answer1);

    const lineStream2 = await getLines("day04/input");
    const answer2 = await getPart2Answer(lineStream2);
    console.log(answer2);
}

if (import.meta.main) await main();

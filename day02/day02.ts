import { assertEquals } from "https://deno.land/std@0.208.0/assert/assert_equals.ts";
import { getLines, mapStream } from "../common/io.ts";

type Colour = string;

class CubeSet extends Map<Colour, number> {
    constructor(config: {[colour: Colour]: number} = {}) {
        super(Object.entries(config));
        this.set("red", this.get("red") ?? 0);
        this.set("green", this.get("green") ?? 0);
        this.set("blue", this.get("blue") ?? 0);
    }

    static fromString(rs: string): CubeSet {
        return new CubeSet(
            Object.fromEntries(rs
                .trim()
                .split(",")
                .map(s => s.trim())
                .map(s => [ s.split(" ")[1], parseInt(s.split(" ")[0]) ]))
        );
    }

    getPower(): number {
        return (this.get("red") ?? 0) * (this.get("green") ?? 0) * (this.get("blue") ?? 0);
    }
}

class Game {
    readonly id: number;
    readonly reveals: CubeSet[];

    constructor(line: string) {
        this.id = parseInt(line.split(":")[0].split(" ")[1]);
        this.reveals = line
            .split(":")[1]
            .split(";")
            .map(rs => CubeSet.fromString(rs));
    }

    possible(actualSet: CubeSet): boolean {
        for (const reveal of this.reveals) {
            for (const [colour, amount] of reveal) {
                if ((actualSet.get(colour) ?? 0) < amount) {
                    return false;
                }
            }
        }
        return true;
    }

    getMinimalSet(): CubeSet {
        const minimalSet: CubeSet = new CubeSet();
        this.reveals.forEach(r => {
            for (const [colour, amount] of r.entries()) {
                minimalSet.set(colour, Math.max(amount, minimalSet.get(colour) ?? 0));
            }
        });
        return minimalSet;
    }
}

Deno.test("parse game", () => {
    const game = new Game("Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green");
    assertEquals(game.id, 1);
    assertEquals(game.reveals, [
        new CubeSet({ blue: 3, red: 4 }),
        new CubeSet({ red: 1, green: 2, blue: 6 }),
        new CubeSet({ green: 2 }),
    ]);
});

async function getPart1Answer(games: ReadableStream<Game>, actualSet: CubeSet) {
    let answer = 0;
    for await (const game of games) {
        if (game.possible(actualSet)) {
            answer += game.id;
        }
    }
    return answer;
}

const ACTUAL_SET = new CubeSet({ red: 12, green: 13, blue: 14 });

const TEST_GAMES = [
    new Game("Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green"),
    new Game("Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue"),
    new Game("Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red"),
    new Game("Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red"),
    new Game("Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"),
];

Deno.test("day 2: part 1 example", async () => {
    const games = ReadableStream.from(TEST_GAMES);
    assertEquals(await getPart1Answer(games, ACTUAL_SET), 8);
});

async function getPart2Answer(games: ReadableStream<Game>) {
    let answer = 0;
    for await (const game of games) {
        const minimalSet = game.getMinimalSet();
        answer += minimalSet.getPower();
    }
    return answer;
}

Deno.test("day 2: part 2 example", async () => {
    const games = ReadableStream.from(TEST_GAMES);
    assertEquals(await getPart2Answer(games), 2286);
});

async function getGames(path: string) {
    return mapStream(await getLines(path), line => new Game(line));
}

async function main() {
    const games1 = await getGames("./day02/input");
    const answer1 = await getPart1Answer(games1, ACTUAL_SET);
    console.log(answer1);

    const games2 = await getGames("./day02/input");
    const answer2 = await getPart2Answer(games2);
    console.log(answer2);
}

if (import.meta.main) await main()

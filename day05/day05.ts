import { assertEquals } from "https://deno.land/std@0.208.0/assert/assert_equals.ts";
import { getLines } from "../common/io.ts";

class Almanac {
    constructor(
        private initialSeedIndication: number[],
        private maps: AlmanacMap[],
    ) { }

    static async from(lines: ReadableStream<string>) {
        const lineReader = lines.getReader();
        const firstLine = (await lineReader.read()).value!;
        const [seedsStr, initialSeedStr] = firstLine.split(":");
        assertEquals(seedsStr, "seeds");
        const initialSeeds = initialSeedStr.trim().split(' ').map(seedId => parseInt(seedId));
        assertEquals((await lineReader.read()).value, '');

        const maps = [];
        let newMap;
        do {
            newMap = await AlmanacMap.from(lineReader);
            if (newMap !== undefined) {
                maps.push(newMap)
            }
        } while (newMap !== undefined)

        return new Almanac(initialSeeds, maps);
    }

    private doTheMapping(values: number[]): number[] {
        let mappingFrom = "seed";
        while (mappingFrom != "location") {
            const map = this.maps.find(m => m.from === mappingFrom);
            if (map === undefined) {throw new Error(`could not find map that maps from ${mappingFrom}`);}
            values = values.map(value => map.map(value));
            mappingFrom = map.to;
        }
        return values;
    }

    findLowestLocationForAnySeed(): number {
        const values = this.doTheMapping(this.initialSeedIndication);
        return Math.min(...values);
    }

    findLowestLocationForAnySeedCorrect(): number {
        let min = Infinity;
        for (let i = 0; i + 1 <= this.initialSeedIndication.length; i += 2) {
            const start = this.initialSeedIndication[i];
            const size = this.initialSeedIndication[i + 1];
            for (let value = start; value < start + size; value++) {
                min = Math.min(min, this.doTheMapping([value])[0]);
                console.log(`subprocessed ${value-start + 1}/${size}`)
            }
            console.log(`processed ${i/2 + 1}/${this.initialSeedIndication.length/2}`)
        }
        return min;
    }
}

class AlmanacMapRule {
    constructor(
        readonly destinationStart: number,
        readonly sourceStart: number,
        readonly range: number,
    ) { }

    private offset(): number {
        return this.destinationStart - this.sourceStart;
    }

    maps(value: number): boolean {
        return value >= this.sourceStart && value < this.sourceStart + this.range;
    }

    map(value: number): number {
        return value + this.offset();
    }
}

class AlmanacMap {
    constructor(
        readonly from: string,
        readonly to: string,
        readonly rules: AlmanacMapRule[],
    ) { }

    static async from(lineReader: ReadableStreamDefaultReader<string>): Promise<AlmanacMap | undefined> {
        const fromToLine = (await lineReader.read()).value;
        if (fromToLine === undefined) return undefined;

        const [from, to] = fromToLine.split(' ')[0].split('-to-');
        const rules = [];
        while (true) {
            const rangeInstruction = (await lineReader.read()).value;
            if (rangeInstruction === undefined || rangeInstruction === '') break;
            const [destinationStart, sourceStart, range] = rangeInstruction.split(' ').map(n => parseInt(n));
            rules.push(new AlmanacMapRule(destinationStart, sourceStart, range));
        }

        return new AlmanacMap(from, to, rules);
    }

    map(value: number): number {
        for (const rule of this.rules) {
            if (rule.maps(value)) {
                return rule.map(value);
            }
        }
        return value;
    }
}

async function getPart1Answer(lines: ReadableStream<string>): Promise<number> {
    const almanac = await Almanac.from(lines);
    return almanac.findLowestLocationForAnySeed();
}

async function getPart2Answer(lines: ReadableStream<string>): Promise<number> {
    const almanac = await Almanac.from(lines);
    return almanac.findLowestLocationForAnySeedCorrect();
}

Deno.test("day 5: part 1 example", async () => {
    const exampleLines = await getLines("day05/example-input");
    assertEquals(await getPart1Answer(exampleLines), 35);
});

Deno.test("day 5: part 2 example", async () => {
    const exampleLines = await getLines("day05/example-input");
    assertEquals(await getPart2Answer(exampleLines), 46);
});

async function main() {
    const lineStream1 = await getLines("day05/input");
    const answer1 = await getPart1Answer(lineStream1);
    console.log(answer1);

    const lineStream2 = await getLines("day05/input");
    const answer2 = await getPart2Answer(lineStream2);
    console.log(answer2);
}

if (import.meta.main) await main();

import { TextLineStream } from "https://deno.land/std@0.208.0/streams/mod.ts";

export async function getLines(path: string) {
    return (await Deno.open(path))
        .readable
        .pipeThrough(new TextDecoderStream())
        .pipeThrough(new TextLineStream());
}

export function mapStream<T, R>(stream: ReadableStream<T>, mapFn: (chunk: T) => R) {
    return stream.pipeThrough(new TransformStream<T, R>({
        transform(chunk, controller) {
            controller.enqueue(mapFn(chunk));
        },
    }));
}
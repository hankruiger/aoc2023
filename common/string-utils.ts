export function reverse(val: string): string {
    return [... val].reverse().join("");
}

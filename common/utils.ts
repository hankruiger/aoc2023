import { assertEquals } from "https://deno.land/std@0.208.0/assert/assert_equals.ts";

export function range(size: number, start = 0) {
    return [...Array(size).keys()].map(n => n + start)
}

Deno.test("test range", () => {
    assertEquals(range(4), [0, 1, 2, 3]);
    assertEquals(range(4, 2), [2, 3, 4, 5]);
});

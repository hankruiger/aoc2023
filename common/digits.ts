import { assertEquals } from "https://deno.land/std@0.208.0/assert/mod.ts";
import { reverse } from "./string-utils.ts";

export const NUMERIC_DIGITS = [... "0123456789"];

export function isDigit(c: string) {
    return NUMERIC_DIGITS.includes(c);
}

export const SPELLED_OUT_DIGITS = {
    "zero": 0,
    "one": 1,
    "two": 2,
    "three": 3,
    "four": 4,
    "five": 5,
    "six": 6,
    "seven": 7,
    "eight": 8,
    "nine": 9,
}

export const ALL_DIGITS = {
    ... Object.fromEntries(NUMERIC_DIGITS.map(d => [d, parseInt(d)])),
    ... SPELLED_OUT_DIGITS,
};

export function findFirstDigit(line: string, digits: {[s: string]: number} = ALL_DIGITS): number {
    let firstIndex: number | null = null;
    let firstDigit: number | null = null;
    for (const [spelledOutDigit, digitValue] of Object.entries(digits)) {
        const foundAt = line.indexOf(spelledOutDigit);
        if (foundAt === -1) continue;
        if (firstIndex == null || foundAt < firstIndex) {
            firstIndex = foundAt;
            firstDigit = digitValue;
        }
    }
    if (firstDigit == null) {
        throw new Error("Could not find any digit.");
    }
    return firstDigit;
}

Deno.test("first digit with words", () => {
    assertEquals(findFirstDigit("12fseklfjseone"), 1);
    assertEquals(findFirstDigit("seven"), 7);
    assertEquals(findFirstDigit("3oneight"), 3);
});

export function findLastDigit(line: string, digits: {[s: string]: number} = ALL_DIGITS): number {
    return findFirstDigit(reverse(line), Object.fromEntries(
        Object.entries(digits).map(([k, v]) => [reverse(k), v])
    ))
}

Deno.test("last digit with words", () => {
    assertEquals(findLastDigit("12fseklfjseone"), 1);
    assertEquals(findLastDigit("seven"), 7);
    assertEquals(findLastDigit("3oneight"), 8);
});

import { assertEquals } from "https://deno.land/std@0.208.0/assert/assert_equals.ts";
import { findFirstDigit, findLastDigit, isDigit } from "../common/digits.ts";
import { getLines } from "../common/io.ts";

function getCalibrationValue(value1: number, value2: number) {
    return 10 * value1 + value2;
}

async function getPart1Answer(lineStream: ReadableStream<string>) {
    let answer = 0;
    for await (const line of lineStream) {
        const chars = [... line];
        const lineDigits = chars
            .filter(c => isDigit(c))
            .map(d => parseInt(d));

        const calibrationValue = getCalibrationValue(lineDigits[0], lineDigits[lineDigits.length - 1]);

        answer += calibrationValue;
    }
    return answer;
}

Deno.test("day 1: part 1 example", async () => {
    const exampleData = ReadableStream.from(
        ["1abc2", "pqr3stu8vwx", "a1b2c3d4e5f", "treb7uchet"]
    );
    assertEquals(await getPart1Answer(exampleData), 142);
})

async function getPart2Answer(lineStream: ReadableStream<string>) {
    let answer = 0;
    for await (const line of lineStream) {
        const firstDigit = findFirstDigit(line);
        const lastDigit = findLastDigit(line);

        const calibrationValue = getCalibrationValue(firstDigit, lastDigit);

        answer += calibrationValue;
    }
    return answer;
}

Deno.test("day 1: part 2 example", async () => {
    const exampleData = ReadableStream.from(
        ["two1nine", "eightwothree", "abcone2threexyz", "xtwone3four", "4nineeightseven2", "zoneight234", "7pqrstsixteen"]
    );
    assertEquals(await getPart2Answer(exampleData), 281);
})

async function main() {
    const lineStream1 = await getLines("day01/input");
    const answer1 = await getPart1Answer(lineStream1);
    console.log(answer1);

    const lineStream2 = await getLines("day01/input");
    const answer2 = await getPart2Answer(lineStream2);
    console.log(answer2);
}

if (import.meta.main) await main()
